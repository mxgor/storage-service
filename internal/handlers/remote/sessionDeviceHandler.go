package handlers

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/common"
	handlers "gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/handlers/common"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/storage-service/internal/model"
)

func CreateSession(c *gin.Context, env *common.Environment) {
	ctx := c.Request.Context()

	authModel := ctx.Value(model.AuthModelKey).(model.AuthModel)

	receipt := handlers.CreateTransactionReciept(ctx, authModel, env)

	if receipt != nil {
		c.Header("Content-Type", "application/jose")
		c.String(200, receipt.Receipt)
		return
	} else {
		_ = handlers.ErrorResponse(c, handlers.InvalidRequest, errors.New(""))
		return
	}
}
