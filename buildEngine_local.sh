#!/bin/bash
rm -rf .engines
mkdir .engines
cd .engines
git clone https://gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/plugins/local-provider.git .local
git clone https://gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/plugins/hashicorp-vault-provider.git .vault

cd .local
rm localProvider_test.go
go build -buildmode=plugin -gcflags="all=-N -l"
cd .. 

cd .vault
rm vaultProvider_test.go
go build -buildmode=plugin -gcflags="all=-N -l"
cd ..
